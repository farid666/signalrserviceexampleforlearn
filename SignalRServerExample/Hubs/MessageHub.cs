﻿using Microsoft.AspNetCore.SignalR;

namespace SignalRServerExample.Hubs
{
    public class MessageHub:Hub
    {
        public async Task SendMessageAsync(string message)//,IEnumerable<string> connectionIds) string groupName qruplarnan isdeyende veririrk
        {
            #region Caller
            //Sadece servere bildiris gonderen client la elaqe qurur gonderdiyim bildirisi ancaq ozum almaq isdeyiremse bunu isdifade edirem
            #endregion

            #region All
            //Servere bagli olan butun clientlara bildirisn gonderir
            #endregion

            #region Others
            //Sadece Servere bildirim gonderen client den basqa difer Servere bagli butun clientlara mesaj gonderir
            #endregion

            #region Hub Clients metodlari
            #region AllExcept
            // -- Bildirilen clientler xaric servere bagli butun clientlere mesaj gonderir
            //await Clients.AllExcept(connectionIds).SendAsync("receiveMessage",message);
            #endregion

            #region Client
            // Servere bagli olan clientlerden hansina isdeyiriskse ona mesaj gondermek ucundu
            //await Clients.Client(connectionId).SendAsync("receiveMessage",message);
            #endregion

            #region Clients
            // Servere bagli olan clientlerden nece client isdeyiriskse onlara mesaj gondermek ucundu
            //await Clients.Clients(connectionIds).SendAsync("receiveMessage",message);
            #endregion

            #region Group
            // Bildirilen qrupdaki clientlara mesaj gonderir
            //1 ci qruplara yaradilmali ve clientlar qruplara subscribe olmalidi
            //await Clients.Group(groupName).SendAsync("receiveMessage",message);
            #endregion

            #region GroupExcept
            // Bildirilen qrupda bildirilen klientler xaricinde hamiya mesaj gonderir
            //await Clients.GroupExcept(groupName,connectionIds).SendAsync("receiveMessage", message);
            #endregion

            #region Groups
            // -- Birden cox qrupdaki clientlara mesaj gondermeye imkan verir
            //await Clients.Groups(groupNames).SendAsync("receiveMessage", message);
            #endregion

            #region OthersInGroup
            // Mesaji gonderen client xaric qrupdaki butun clientlere mesaj gonderir
            //await Clients.OthersInGroup(groupName).SendAsync("receiveMessage", message);
            #endregion

            #region User
            // Authentication olmus usere eriwmeyi sagliyir
            #endregion

            #region Users
            // Authentication olmus userlere eriwmeyi sagliyir
            #endregion


            #endregion

            await Clients.All.SendAsync("receiveMessage",message);
        }

        public override async Task OnConnectedAsync()
        {
            await Clients.Caller.SendAsync("getConnectionId",Context.ConnectionId);
        }

        //client terefde AddGroup metodunu invoke edib deyerleri gondermek lazimdi
        public async Task AddGroup(string connectionId,string groupName)
        {
            await Groups.AddToGroupAsync(connectionId,groupName);
        }
    }
}
