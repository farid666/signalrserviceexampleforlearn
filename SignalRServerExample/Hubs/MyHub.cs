﻿using Microsoft.AspNetCore.SignalR;
using SignalRServerExample.Interfaces;

namespace SignalRServerExample.Hubs
{
    public class MyHub:Hub<IMessageClient> // burda IMessageClient interface yaratdim icinde SendAsync icinde client terefde oxunan metodlarin adlarina uygun metodlar yaratdimki burda await Clients.All.SendAsync("clients",clients); dirnaqlar icinde metodun adini sehv yaza bilerem diqqetsizlik edib 
    {//yuxarda interfacede metodlari teyin etmek Strongly Typed Hubs adlanir
        static List<string> clients = new List<string>();

        //bu metodu business mentiqine dasimaq ucun IHubContext isdifade eledim
     /*   public async Task SendMessageAsync(string message)
        {
            await Clients.All.SendAsync("ReceiveMessage",message);
        }*/


        public override async Task OnConnectedAsync()
        {
            clients.Add(Context.ConnectionId);
            /*         await Clients.All.SendAsync("clients",clients); 
                     await Clients.All.SendAsync("userJoined", Context.ConnectionId);n*/
            await Clients.All.Clients(clients);//yuxarda interfacede metodlari teyin etdikden sonra SendAsync icinde vermeye ehtiyac qalmir
            await Clients.All.UserJoined(Context.ConnectionId);
        }


        public override async Task OnDisconnectedAsync(Exception? exception)
        {
            clients.Remove(Context.ConnectionId);
            /* await Clients.All.SendAsync("clients",clients);
             await Clients.All.SendAsync("userLeaved", Context.ConnectionId);*/
            await Clients.All.Clients(clients);//yuxarda interfacede metodlari teyin etdikden sonra SendAsync icinde vermeye ehtiyac qalmir
            await Clients.All.UserLeaved(Context.ConnectionId);
        }
    }
}
